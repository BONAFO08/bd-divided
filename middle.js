const db = require('./conexion');

const creation = (name, type, id) => {
    let aux;

    let people = {
        name: name,
        type: type,
        idRast: id
    };

    switch (type) {
        case "Spirit":
            aux = new db.Spirit(people);
            aux.save();
            console.log(`Espiritu ${people.name}`);
            break;

        case "Goddess":
            aux = new db.Goddess(people);
            aux.save();
            console.log(`Diosa ${people.name}`);
            break;

        case "Human":
            aux = new db.Human(people);
            aux.save();
            console.log(`Humano ${people.name}`);
            break;
    }
}



module.exports = {
    creation,
};