const mongoose = require('mongoose');
const mongoUrl = `mongodb://localhost:27017/baseMuerta`;
const schemaSpirit = require('./model');
const schemaGoddess = require('./modeldos');
const schemaHuman = require('./modeldos');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}


mongoose.connect(mongoUrl, options);

const Spirit = mongoose.model('spirits', schemaSpirit);
const Goddess = mongoose.model('goddesses', schemaGoddess);
const Human = mongoose.model('humans', schemaHuman);


module.exports = {
    mongoose,
    Spirit,
    Goddess,
    Human
};